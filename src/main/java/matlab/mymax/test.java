package matlab.mymax;

import mymax.*;
import com.mathworks.toolbox.javabuilder.*;

public class test {

	public static void main(String[] args) throws MWException {
		//caling the matlab function

		MWNumericArray[] n = new MWNumericArray[5];
		
		Object[] result = null;
		
		int max = 0;
		
		int[] arguments = { 23, 24, 25, 100, 9};
				
		n[0] = new MWNumericArray(arguments[0]);
		n[1] = new MWNumericArray(arguments[1]);
		n[2] = new MWNumericArray(arguments[2]);
		n[3] = new MWNumericArray(arguments[3]);
		n[4] = new MWNumericArray(arguments[4]);
		
		Class1 class1 = new Class1();
		
		result = class1.mymax(1, n);
		
		System.out.println(result[0]);
	}

}
