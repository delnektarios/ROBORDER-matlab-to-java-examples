package matlab.quadratic;

import com.mathworks.toolbox.javabuilder.*;

import quadratic.Class1;

/**
 * 
 *
 */
public class App 
{
	
	private static int param_num = 3;
	
    public static void main( String[] args )
    {
    	MWNumericArray[] parameters = new MWNumericArray[param_num];
    	int[] arguments = { 2, 2, 2, 100, 9};
    	Object[] results = null;
    	Class1 matlab = null;

    	for(int i=0; i<param_num; i++){
    		parameters[i] = new MWNumericArray(arguments[i], MWClassID.DOUBLE);
    	}
    	
    	for(int i=0;i<param_num; i++){
    		System.out.println(parameters[i]);
    	}
		
		try {
			matlab = new Class1();
			results = matlab.quadratic(2, parameters);
			
			System.out.println(results[0] + " ," + results[1]);
		} catch (MWException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			MWArray.disposeArray(parameters);
		    MWArray.disposeArray(results);
		    matlab.dispose();
		}
		
		
    }
}
