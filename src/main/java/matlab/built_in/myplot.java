package matlab.built_in;

import com.mathworks.toolbox.javabuilder.MWArray;
import com.mathworks.toolbox.javabuilder.MWException;
import com.mathworks.toolbox.javabuilder.MWNumericArray;


import plot.Class1;



public class myplot {

	private static final int param_num = 1;
	
	public static void main(String[] args) {
    	MWNumericArray[] parameters = new MWNumericArray[param_num];
    	int[] arguments = { 3, 2, 2, 100, 9};
    	Object[] results = null;
    	Class1 myplot = null;
    	int n = 20;                /* Number of points to plot */
        MWNumericArray x = null;   /* Array of x values */
        MWNumericArray y = null;   /* Array of y values */
        Class1 thePlot = null;    /* Plotter class instance */
    	
    	for(int i=0; i<param_num; i++){
    		parameters[i] = new MWNumericArray(arguments[i]);
    	}
    	
    	for(int i=0; i<param_num; i++){
    		System.out.println(parameters[i]);
    	}
    	
		try {
		
	         /* Create new plotter object */
	         thePlot = new Class1();

	         /* Plot data */
		 /*the function is a sin(x) where x is a single number*/
		 /*CHECK this.param_num IS SET TO 1*/
	         thePlot.myplot(parameters);
	         thePlot.waitForFigures(); 

		} catch (MWException e) {

			e.printStackTrace();
		}finally{
			MWArray.disposeArray(parameters);
		    MWArray.disposeArray(results);
		    if(myplot!=null){
		    	myplot.dispose();
		    }
		}

	}

}
